

#include <iostream>
#include <sstream>
#include <climits>
#include <cstdlib>
#include <vector>
#include <map>
#include <set>
#include <functional>
#include <queue>

using namespace std;

//would do typename typedef but i wouldnt be able to use vector<Pos> bcs Pos wouldnt be constructible without values

enum class Tile{
    kSTART,
    kEND,
    kOPENED,
    kPATH
};

void printTile(const Tile tile){
    if (tile==Tile::kSTART) {
        cout<<"S";
    } else if(tile==Tile::kEND){
        cout<<"E";
    } else if(tile==Tile::kOPENED){
        cout<<".";
    } else if(tile==Tile::kPATH){
        cout<<"o";
    }
}

struct Pos {
    Pos(){
        m_vertical=0;
        m_horizontal=0;
    }
    Pos(const int x, const int y) {
        this->m_vertical=x;
        this->m_horizontal=y;
    }
    bool is_next_to(const Pos & pos) const {
        if ((m_vertical==pos.m_vertical && abs(m_horizontal-pos.m_horizontal)==1) || (m_horizontal==pos.m_horizontal && abs(m_vertical-pos.m_vertical)==1)) {
            return true;
        } else return false;
    }
    
    int DistanceTo(const Pos & rhs) const {
        return abs(m_horizontal - rhs.m_horizontal) + abs(m_vertical - rhs.m_vertical) ;
    }
    
    
    //for ordering in set
    bool operator < (const Pos & rhs) const {
        if (m_vertical==rhs.m_vertical) {
            return m_horizontal<rhs.m_horizontal;
        } else return m_vertical<rhs.m_vertical;
    }
    
    bool operator == (const Pos & rhs) const {
        return m_vertical==rhs.m_vertical && m_horizontal==rhs.m_horizontal ;
    }
    
    void GetNegborsInto(vector<Pos> & neighbours) const {
        neighbours.clear();
        
        neighbours.push_back(Pos(m_vertical+1, m_horizontal));
        neighbours.push_back(Pos(m_vertical, m_horizontal+1));
        neighbours.push_back(Pos(m_vertical-1, m_horizontal));
        neighbours.push_back(Pos(m_vertical, m_horizontal-1));
        
    }
    
    int m_vertical;
    int m_horizontal;
};


struct AStarPos{
    Pos m_pos;
    int m_distance_from_start;
};





class Table {
public:
    void run(){
        load();
            cout<<" for bfs enter : b ";
            cout<<" for dfs enter : d ";
            cout<<" for gready enter : g ";
            cout<<" for Astar enter : a ";
            cout<<" for loading new table enter : l ";
            cout<<" for Exiting enter : e ";
            char c;
            cin>>c;
            
           
            if (c=='l'){
                load();
            } else if (c=='b'){
                Bfs();
            } else if (c=='d'){
                m_searchRun.insert(pair<Pos, pair<const Pos, Tile>> (m_start, make_pair(m_start, Tile::kSTART)));
                Dfs(m_start);
            } else if (c=='g'){
                GreadySearch();
            } else if (c=='a'){
                AStarSearch();
            }
            RecenstructPath();
            Print_table();
            m_searchRun.clear();

    }
    
    
    
    
    void load(){
        m_fields.erase(m_fields.cbegin(), m_fields.cend());
        string line;
        
        for (int i = 0,j=0; getline(cin, line); i++) {
            bool double_break=false;
            vector<bool> bool_line;
            for (auto c : line) {
                //it means end of description for map now postion for map
                if (c=='s') {
                    m_length=i;
                    double_break=true;
                    break;
                }
                bool_line.emplace_back(c=='X');
                j++;
            }
            if (double_break) {
                break;
            }
            m_fields.emplace_back(bool_line);
            m_width=(int)line.size();
        }
        
        int i=0,j=0;
        stringstream ss(line);
        string dump;
        ss>>dump>>i>>dump>>j;
        m_start.m_horizontal=i;
        m_start.m_vertical=j;
        
        cin>>dump>>i>>dump>>j;
        m_end.m_horizontal=i;
        m_end.m_vertical=j;
    }
    
    void Print_table(){
        cout<<endl;
        for (int i=0; i<m_length; i++) {
            for (int j=0; j<m_width; j++) {
                auto find=m_searchRun.find(Pos(i,j));
                if (m_start==Pos(i,j)) {
                    cout<<"S";
                } else if (m_end==Pos(i,j)){
                    cout<<"E";
                } else if (find!=m_searchRun.end()) {
                    printTile(find->second.second);
                } else {
                    if (m_fields[i][j]) {
                        cout<<"X";
                    } else cout<<" ";
                }
            }
            cout<<endl;
        }
    }
    
private:
    
    int DistanceValueToEnd(const Pos & pos) const {
        return abs(pos.m_horizontal - m_end.m_horizontal) + abs(pos.m_vertical - m_end.m_vertical) ;
    }
     
    
    bool ViablePositon(Pos pos) const {
        return pos.m_vertical<m_length && pos.m_horizontal<m_width && pos.m_vertical>=0 && pos.m_horizontal>=0;
    }
    
vector<Pos> GetReachableNeigbours(const Pos & pos) const {
        vector<Pos> itter;
        vector<Pos> res;
        pos.GetNegborsInto(itter);
    
        for (const auto & n : itter) {
            if (!ViablePositon(n) || m_fields[n.m_vertical][n.m_horizontal] ) {
                continue;
            } else if (m_searchRun.find(n)!=m_searchRun.end()){
                continue;
            }
            res.push_back(n);
        }
    
        return res;
    }
    
    bool Dfs(const Pos & pos){
    
        if (pos==m_end) {
            return true;
        }
        
        for (auto && p : GetReachableNeigbours(pos)) {
            m_searchRun.insert(pair<Pos, pair<const Pos, Tile>> (p, make_pair(pos, Tile::kOPENED)));
            Print_table();
            if (Dfs(p)) {
                return true;
            }
        }
        
  
        
        return false;
    }
    
    bool Bfs(){
        vector<Pos> queue;
        queue.push_back(m_start);
        m_searchRun.insert(pair<Pos, pair<const Pos, Tile>> (m_start, make_pair(m_start, Tile::kOPENED)));
        
        for (int queue_idx=0; queue_idx<queue.size(); queue_idx++) {
            Pos pos=queue[queue_idx];
    
            
            if (pos==m_end) {
                return true;
            }
            
            for (auto && n : GetReachableNeigbours(pos)) {
                
                m_searchRun.insert(pair<Pos, pair<const Pos, Tile>> (n, make_pair(pos, Tile::kOPENED)));
                queue.push_back(n);
            }
            
            Print_table();
        }
        
        
        return false;
    }
    
    bool RandomSearch(){
        vector<Pos> queue;
        int erased=0;
        queue.push_back(m_start);
        m_searchRun.insert(pair<Pos, pair<const Pos, Tile>> (m_start, make_pair(m_start, Tile::kOPENED)));
    
        
        for (int queue_idx=0; queue_idx<queue.size()+erased; queue_idx++) {
            int random=rand()%queue.size();
            Pos pos=queue[random];
            queue.erase(queue.cbegin()+random);
            erased++;
    
            if (pos==m_end) {
                return true;
            }
            
            for (auto && n : GetReachableNeigbours(pos)) {
                
                m_searchRun.insert(pair<Pos, pair<const Pos, Tile>> (n, make_pair(pos, Tile::kOPENED)));
                queue.push_back(n);
            }
            
            Print_table();
        }
        
        return false;
    }
    
    bool GreadySearch(){
        Pos end=m_end;
        //Using menhatend distance becouse priority_queue pop from back and not front need to order in reverse sou smallest value is on end of priority_queue
        auto cmp = [end](const Pos & lfs, const Pos & rhs) { return lfs.DistanceTo(end) > rhs.DistanceTo(end); };
        priority_queue<Pos, vector<Pos>, decltype(cmp)> queue(cmp);
        queue.push(m_start);
        m_searchRun.insert(pair<Pos, pair<const Pos, Tile>> (m_start, make_pair(m_start, Tile::kOPENED)));
        
        
        while (queue.size()!=0) {
            Pos pos=queue.top();
            queue.pop();
    
            if (pos==m_end) {
                return true;
            }
            
            for (auto && n : GetReachableNeigbours(pos)) {
                
                m_searchRun.insert(pair<Pos, pair<const Pos, Tile>> (n, make_pair(pos, Tile::kOPENED)));
                queue.push(n);
            }
            
            Print_table();
        }
        
        return false;
    }
    
    bool AStarSearch(){
        Pos end=m_end;
        //Using Manhattan distance. becouse priority_queue pop from back and not front need to order in reverse sou smallest value is on end of priority_queue
        auto cmp = [end](const AStarPos & lfs, const AStarPos & rhs) { return lfs.m_pos.DistanceTo(end) + lfs.m_distance_from_start > rhs.m_pos.DistanceTo(end) + rhs.m_distance_from_start; };
        priority_queue<AStarPos, vector<AStarPos>, decltype(cmp)> queue(cmp);
        
        AStarPos a_start;
        a_start.m_pos=m_start;
        a_start.m_distance_from_start=0;
        queue.push(a_start);
        m_searchRun.insert(pair<Pos, pair<const Pos, Tile>> (m_start, make_pair(m_start, Tile::kOPENED)));
        
        while (queue.size()!=0) {
            AStarPos pos=queue.top();
            queue.pop();
    
            if (pos.m_pos==m_end) {
                return true;
            }
            
            for (auto && n : GetReachableNeigbours(pos.m_pos)) {
                
                AStarPos insert;
                insert.m_pos=n;
                insert.m_distance_from_start=pos.m_distance_from_start+1;
                
                m_searchRun.insert(pair<Pos, pair<const Pos, Tile>> (n, make_pair(pos.m_pos, Tile::kOPENED)));
                queue.push(insert);
            }
            
            Print_table();
        }
        
        return false;
    }
    
    bool PathToStart(set<Pos> & positions, const Pos & pos) {
        if (pos==m_start) {
          
        }
        return true;
    }
    
    void RecenstructPath(){
        m_searchRun.find(m_start)->second.second=Tile::kSTART;
        auto find=m_searchRun.find(m_end)->second;
        
        find.second=Tile::kEND;
        Pos pos=find.first;
        
        while (true) {
            if (pos==m_start) {
                break;
            }
            auto tmp=&m_searchRun.find(pos)->second;
            tmp->second=Tile::kPATH;
            pos=tmp->first;
        }
    }
    
    
    int m_width;
    int m_length;
    Pos m_start;
    Pos m_end;
    map<Pos, pair<const Pos, Tile>> m_searchRun;
    vector<vector<bool>> m_fields;
};


int main(int argc, const char * argv[]) {
    Table table;
    table.run();
    return 0;
}

